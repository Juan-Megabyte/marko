import os

proc quickBackup(): proc =
  # Getting the user's quickmarks
  let quickmarks = getHomeDir() & ".config/qutebrowser/quickmarks"
  let q = open(quickmarks)

  # Creating the directory for the back-up if it doesn't exist
  # If it does exist then overwrite the current backup
  if dirExists(getHomeDir() & ".config/marko/bak/"):
    echo "Directory exists overwriting quickmark backup file!"

    let filename:string = getHomeDir() & ".config/marko/bak/quickmarks"
    let bak = open(filename, fmWrite)
    bak.writeLine(readAll(q))

    echo "Finished!"
  else:
    echo "Directory does not exist!"
    echo "Creating one..."

    var createdDir:string = "mkdir -p " & getHomeDir() & ".config/marko/bak"

    discard execShellCmd(createdDir)

    echo "Now creating bak of quickmarks"

    let filename:string = createdDir & "/quickmarks"
    let bak = open(filename, fmWrite)
    bak.writeLine(readAll(q))

    echo "Finished!"

proc quickread(): proc =
      echo "Here is the current list of quickmarks!\n"

      # Gets the current list of quickmarks
      let quickmarks = getHomeDir() & ".config/qutebrowser/quickmarks"
      let q = open(quickmarks)
      echo readAll(q)

proc quickReplace(): proc =
      let
        quickmarks = getHomeDir() & ".config/qutebrowser/quickmarks"
        backup = getHomeDir() & ".config/marko/bak/quickmarks"
      # Here I determine if there is already a quickmark file present, if it is validate to make sure info doesn't get lost
      # Should the file not exist the backup gets copied there.
      if fileExists(quickmarks):
        echo "You already have a quickmark file are you sure you want to replace it? (y/n)"

        case readLine(stdin)
        of "y":
          echo "Transfering File from Bak...\n"
          copyFile(backup, quickmarks)
          echo "You now have a new quickmark file!\n"
        of "n": echo "I like your choice!"
      else:
          copyFile(backup, quickmarks)

var command = ""

try:
  let args = commandLineParams()
  command = args[0]
except:
  command = "-h"


case command
of "-h", "--help":
  echo """
  Marko is a bookmark manager for qutebrowser:

  Usage:
  marko -h | --help
  marko -b | --backup
  marko -p [<quickmark file>] WOP
  marko -r | --read

  Options:
  -h, --help        Shows this page.
  -b, --backup      Creates a backup of your current quickmarks.
  -p                Replaces your current quickmarks with a new file.
  -r, --read        Reads your current quickmark file.
  """
of "-b", "--backup":
  quickBackup()
of "-p":
  quickReplace()
of "-r","--read":
  quickRead()
