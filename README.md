# Marko
## The qutebrowser quickmark manager

This is a quickmark manager for the qutebrowser that I created. It's written in Nim and will contain more features than currently present, this project will also serve as a DB where I keep my quickmarks stored.

### List of features

- [X] Creates a backup of current quickmarks.
- [X] Able to import quickmarks into qutebrowser.
- [X] Reads qutebrowser's current quickmarks

### Usage

If you want to use this app just copy the marko executable into your PATH and use the command marko in your console.

``` bash
marko -h
```

This command brings up the help menu.
